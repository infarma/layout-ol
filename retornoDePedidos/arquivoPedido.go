package retornoDePedidos

import (
	"bufio"
	"os"
	"strconv"
)

type ArquivoDePedido struct {
	ItensDoPedido []ItensDoPedido `json:"ItensDoPedido"`
}

func retornaComoInt64(valor string) int64 {
	vl, _ := strconv.ParseInt(valor, 10, 10)
	return vl
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "1":
			var registroTemp ItensDoPedido
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.ItensDoPedido = append(arquivo.ItensDoPedido, registroTemp)
		}
	}
	return arquivo, err
}
