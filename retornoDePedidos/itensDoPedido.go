package retornoDePedidos

import (
	"errors"
	"strings"
)

type ItensDoPedido struct {
	TipoDoRegistro       int64  `json:"TipoDoPedido"`
	NrPedido             string `json:"NrPedido"`
	CodigoEan            string `json:"CodigoEan"`
	DescricaoDoProduto   string `json:"DescricaoDoProduto"`
	Sequencia            string `json:"Sequencia"`
	ValorUnitario        string `json:"ValorUnitario"`
	PercentualDeDesconto string `json:"PercentualDeDesconto"`
	QuantidadeItem       string `json:"QuantidadeItem"`
	QuantidadeAtendida   string `json:"QuantidadeAtendida"`
	QuantidadeCancelada  string `json:"QuantidadeCancelada"`
	ValorItem            string `json:"ValorItem"`
}

func (c *ItensDoPedido) ComposeStruct(fileContents string) error {
	itens := strings.Split(fileContents, ";")

	if len(itens) >= 16 {
		c.TipoDoRegistro = retornaComoInt64(itens[0])
		c.NrPedido = itens[1]
		c.CodigoEan = itens[2]
		c.DescricaoDoProduto = itens[3]
		c.Sequencia = itens[4]
		c.ValorUnitario = itens[5]
		c.PercentualDeDesconto = itens[6]
		c.QuantidadeItem = itens[7]
		c.QuantidadeAtendida = itens[8]
		c.QuantidadeCancelada = itens[9]
		c.ValorItem = itens[10]
	} else {
		return errors.New("Erro")
	}

	return nil
}
