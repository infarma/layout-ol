package envioDePedidos

import (
	"errors"
	"strings"
)

type ItensDoPedido struct {
	TipoDoRegistro       int64  `json:"TipoDoRegistro"`
	NrPedido             string `json:"NrPedido"`
	CodigoEan            string `json:"CodigoEan"`
	CodigoDoProduto      string `json:"CodigoDoProduto"`
	DescricaoDoProduto   string `json:"DescricaoDoProduto"`
	Sequencia            string `json:"Sequencia"`
	ValorUnitario        string `json:"ValorUnitario"`
	PercentualDeDesconto string `json:"PercentualDeDesconto"`
	QuantidadeItem       string `json:"QuantidadeItem"`
	QuantidadeAtendida   string `json:"QuantidadeAtendida"`
	QuantidadeCancelada  string `json:"QuantidadeCancelada"`
	ValorItem            string `json:"ValorItem"`
}

func (c *ItensDoPedido) ComposeStruct(fileContents string) error {
	itens := strings.Split(fileContents, ";")

	if len(itens) >= 16 {
		c.TipoDoRegistro = retornaComoInt64(itens[0])
		c.NrPedido = itens[1]
		c.CodigoEan = itens[2]
		c.CodigoDoProduto = itens[3]
		c.DescricaoDoProduto = itens[4]
		c.Sequencia = itens[5]
		c.ValorUnitario = itens[6]
		c.PercentualDeDesconto = itens[7]
		c.QuantidadeItem = itens[8]
		c.QuantidadeAtendida = itens[9]
		c.QuantidadeCancelada = itens[10]
		c.ValorItem = itens[11]
	} else {
		return errors.New("Erro")
	}

	return nil
}
