package envioDePedidos

import (
	"errors"
	"strings"
)

type CabecalhoDoPedido struct {
	TipoDoRegistro           int64  `json:"TipoDoRegistro"`
	CnpjParaFaturamento      string `json:"CnpjParaFaturamento"`
	NomeFantasia             string `json:"NomeFantasia"`
	NomeCliente              string `json:"NomeCliente"`
	InscricaoEstadualCliente string `json:"InscricaoEstadualCliente"`
	EnderecoCliente          string `json:"EnderecoCliente"`
	BairroCliente            string `json:"BairroCliente"`
	CidadeCliente            string `json:"CidadeCliente"`
	UfCliente                string `json:"UfCliente"`
	CepCliente               string `json:"CepCliente"`
	TelefoneCliente          string `json:"TelefoneCliente"`
	ContatoCliente           string `json:"ContatoCliente"`
	CondPagto                string `json:"CondPagto"`
	NrPedido                 string `json:"NrPedido"`
	ValorPedido              string `json:"ValorPedido"`
	TabelaPreco              string `json:"TabelaPreco"`
	Observacao               string `json:"Observacao"`
	CnpjOperadorLogistico    string `json:"CnpjOperadorLogistico"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	itens := strings.Split(fileContents, ";")

	if len(itens) >= 16 {
		c.TipoDoRegistro = retornaComoInt64(itens[0])
		c.CnpjParaFaturamento = itens[1]
		c.NomeFantasia = itens[2]
		c.NomeCliente = itens[3]
		c.InscricaoEstadualCliente = itens[4]
		c.EnderecoCliente = itens[5]
		c.BairroCliente = itens[6]
		c.CidadeCliente = itens[7]
		c.UfCliente = itens[8]
		c.CepCliente = itens[9]
		c.TelefoneCliente = itens[10]
		c.ContatoCliente = itens[11]
		c.CondPagto = itens[12]
		c.NrPedido = itens[13]
		c.ValorPedido = itens[14]
		c.TabelaPreco = itens[15]
		c.Observacao = itens[16]
		c.CnpjOperadorLogistico = itens[17]
	} else {
		return errors.New("Erro")
	}

	return nil
}
