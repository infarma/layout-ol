package layout_paodeacucar

import (
	"bitbucket.org/infarma/layout-ol/envioDePedidos"
	"bitbucket.org/infarma/layout-ol/retornoDePedidos"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (envioDePedidos.ArquivoDePedido, error) {
	return envioDePedidos.GetStruct(fileHandle)
}

func GetArquivoDeRetornoDePedido(fileHandle *os.File) (retornoDePedidos.ArquivoDePedido, error) {
	return retornoDePedidos.GetStruct(fileHandle)
}
